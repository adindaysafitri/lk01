import java.util.ArrayList;

public class Kereta {
    String nama [] = new String [1000];
    String asal[] = new String [1000];
    String tujuan[] = new String[1000];
    int a = 0; //variabel a untuk identifikasi panjang data (nama, asal, tujuan) pada KAJJ
    ArrayList<String> namaPenumpang = new ArrayList<>(); //untuk menyimpan data nama penumpang pada komuter
    String namaKereta;
    int jumlahTiket; //jmlh tiket komuter
    int jumlahTiket1; //jmlh tiket KAJJ

    //default method Kereta() untuk komuter
    public Kereta(){
        this.jumlahTiket = 1000;
    }

    //overload method Kereta() untuk KAJJ
    public Kereta(String namaKereta, int jumlahTiket1) {
        this.namaKereta = namaKereta;
        this.jumlahTiket1 = jumlahTiket1;
    }

    //default method tambahTiket() untuk komuter
    public void tambahTiket(String nama) { 
        if (jumlahTiket <= 1000){
            namaPenumpang.add(nama);
            System.out.println("==================================================================");
            System.out.println("\t\t\tTiket berhasil dipesan!");
            }
            jumlahTiket--;
    }

    //overload method tambahTiket() untuk KAJJ
    public void tambahTiket(String nama, String asal, String tujuan) {

        if (jumlahTiket1 <= 1000 && jumlahTiket1 > 0){
            this.nama[a] = nama;
            this.asal[a] = asal;
            this.tujuan[a] = tujuan;
            System.out.println("==================================================================");
            System.out.print("Tiket berhasil dipesan! ");
            a++;
            jumlahTiket1--;

            if (jumlahTiket1 < 30 && jumlahTiket1 > 0){
                System.out.println("Sisa tiket tersedia: " + jumlahTiket1);
            }
        }
                else if (jumlahTiket1 <= 0){
                System.out.println("==================================================================");
                System.out.println("Kereta telah habis dipesan, silakan cari jadwal keberangkatan lainnya");
                System.out.print("==================================================================");
            }
    }

    public void tampilkanTiket() {
        if (jumlahTiket != 0){
        System.out.print("==================================================================");
        System.out.println("\nDaftar penumpang kereta api komuter: ");
        System.out.println("------------------------------------");
        for (String nama : namaPenumpang){ //perulangan mencetak setiap nama dalam ArrayList namaPenumpang
            System.out.println("Nama: " + nama);}
            System.out.println(); //memberi spasi/break
        }
            else if (jumlahTiket1 == 0){ // ==0 karena tiket KAJJ habis
            System.out.println("\nDaftar penumpang kereta api " +namaKereta+ ": ");
            System.out.println("-------------------------------------");
            for(int i=0; i<a; i++){ //perulangan mencetak setiap nama, asal, tujuan pada data KAJJ
            System.out.println("Nama: " + nama[i]);
            System.out.println("Asal: " + asal[i]);
            System.out.println("Tujuan: " + tujuan[i]);
            System.out.println("-------------------------------------");
            }
        }
    }
}